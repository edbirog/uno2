package com.networking.uno;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HomeSectionFragment extends Fragment{
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_section_home, container, false);
        // Demonstration of a collection-browsing activity.
        rootView.findViewById(R.id.button2)
        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CustomizedListView.class);
                startActivity(intent);
            }
        });
        rootView.findViewById(R.id.button3)
        .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SectionFragment10Ways.class);
                startActivity(intent);
            }
        });
        rootView.findViewById(R.id.button5)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), VideoWallDemoActivity.class);
                        startActivity(intent);
                    }
                });
        
        
        return rootView;
        
    }
}
